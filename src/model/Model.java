package model;

import controller.Controller;
import view.View;

import java.util.Observable;

public class Model extends Observable {
    String valeur = new String();

    public String getValeur(){
        return (String) this.valeur;
    }


    public void setValeur(String valeur){
        this.valeur = valeur;
        System.out.println("Nouvelle valeur : "+ valeur);
    }

   /* public void startCompteur(View ecran) {
        for(int idx =0; idx < 24; idx++){
            String val = String.format("%d\n",idx);
            System.out.printf(val);
            ecran.getField().setText(val);
            ecran.getField().addNotify();
            try {
                Thread.sleep(5*1000);
            } catch (InterruptedException e) {
                //throw new RuntimeException(e);
                e.printStackTrace();
            }
        }
    }*/
}