package model;

import view.View;

public class Boucle implements Runnable {
    View ecran;
    public Boucle(View ecran) {
        this.ecran = ecran;
    }

    @Override
    public void run(){
        for(int idx =0; idx <5; idx++){
            String val = String.format("%d",idx);
            System.out.printf(val + "\n");
            ecran.getField().setText(val);
            try {
                Thread.sleep(5*1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        ecran.getButtonStart().setEnabled(true);
    }
}