import controller.Controller;
import model.Model;
import view.View;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        view.init();
        Model model = new Model();
        new Controller(model, view);
    }
}