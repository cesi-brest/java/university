package controller;

import model.Boucle;
import model.Model;
import view.View;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;


public class Controller implements ActionListener {
    Model model;
    View ecran;

    public Controller(Model model, View view){
        this.model = model;
        this.ecran = view;
        this.ecran.getButtonStart().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e){
        JButton source = (JButton)e.getSource();
        source.setEnabled(false);
        //this.model.startCompteur(ecran);
        Thread t1 = new Thread((new Boucle(getEcran())));
        t1.start();

    }

    public View getEcran() {
        return ecran;
    }
    public void setEcran(View ecran){
        this.ecran = ecran;
    }
}