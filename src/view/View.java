package view;

import javax.swing.*;
import java.awt.*;

public class View {
    JFrame frame;
    JTextField field;
    JButton buttonStart;

    public JButton getButtonStart() {
        return buttonStart;
    }
    public JFrame getFrame() {
        return frame;
    }
    public JTextField getField() {
        return field;
    }

    public void init(){
        frame = new JFrame("Start");
        frame.setResizable(false);
        frame.add(this.getPanelEcran(), BorderLayout.NORTH);
        frame.add(this.getPanelStart(), BorderLayout.SOUTH);
        frame.setVisible(true);
        frame.pack();
    }

    private Component getPanelStart() {
        JPanel panel = new JPanel();
        buttonStart = new JButton("Start");
        panel.add(buttonStart);
        return panel;
    }

    private Component getPanelEcran() {
        JPanel panel = new JPanel();
        field = new JTextField();
        field.setColumns(10);
        panel.add(field);
        return panel;
    }
}